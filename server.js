//! Deps
const express = require("express");
const helmet = require("helmet");
const compression = require("compression");
const cors = require("cors");
const morgan = require("morgan");
const chalk = require("chalk");
const hbs = require("express-handlebars");
const UA = require("ua-parser-js");
const exec = require("shell-exec");
const simpleIcons = require("simple-icons");

//const con = require('./constants');
let { port, hostname } = {
  port: 3621
  //hostname: '67.182.206.28'
};

const app = express();

app.engine(
  "hbs",
  hbs({
    extname: "hbs",
    defaultView: "default",
    helpers: {
      ifeq: function(a, b, options) {
        if (a === b) {
          return options.fn(this);
        }
      }
    }
  })
);

app.set("view engine", "hbs");
app.set("json spaces", 4);
app.use("/assets", express.static("./assets"));
app.set("view options", {
  layout: false
});
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true
  })
);
app.use(helmet());
app.use(compression());
app.use(cors());
// Logging
app.use(
  morgan((tokens, req, res) => {
    return [
      chalk.hex("#34ace0").bold(`[ ${tokens.method(req, res)} ]`),
      chalk.hex("#ffb142").bold(tokens.status(req, res)),
      chalk.hex("#ff5252").bold(req.hostname + tokens.url(req, res)),
      chalk.hex("#2ed573").bold(tokens["response-time"](req, res) + "ms"),
      chalk.hex("#f78fb3").bold("@ " + tokens.date(req, res))
    ].join(" ");
  })
);

/* app.use([ '/portfolio', '/p' ], require('./routes/portfolio'));
app.use('/lore', require('./routes/lore'));
app.use('/art', require('./routes/art')); */
//app.use('/p', require('./routes/p'));

module.exports = client => {
  app.get("/", async (req, res) => {
    res.render("index", {
      layout: "main",
      name: `ry/`,
      host: req.hostname,
      portfolio: {
        code: [
          {
            name: "portfolio",
            section: "portfoliosec",
            id: "portfolio",
            source: "/portfolio",
            tags: ["Node", "Handlebars", "SASS"]
          },
          {
            name: "homepage",
            section: "homepagesec",
            id: "homepage",
            source: "/homepage",
            tags: ["Node", "Handlebars", "SCSS"]
          },
          {
            name: "yugen.work",
            section: "yugensec",
            id: "yugen",
            source: "/yugen",
            tags: ["Node", "Handlebars", "SCSS"]
          },
          {
            name: "sor.dog",
            section: "sorsec",
            id: "sor",
            source: "/sor",
            tags: ["Node", "Handlebars", "SCSS"]
          },
          {
            name: "benji.monster",
            section: "benjisec",
            id: "benji",
            source: "/benji",
            tags: ["Node", "Handlebars", "SCSS"]
          },
          {
            name: "thaldrin",
            section: "thaldrinsec",
            id: "thaldrin",
            source: "/thaldrin",
            tags: ["Node", "Handlebars", "SCSS", "Discord"]
          },
          {
            name: "kaito",
            section: "kaitosec",
            id: "kaito",
            source: "/kaito",
            tags: ["Node", "Twitter", "Discord"]
          },
          {
            name: "yiff",
            section: "yiffsec",
            id: "yiff",
            source: "/yiff",
            tags: ["Node", "NPM"]
          }
        ]
      }
    });
  });

  app.get("/portfolio", async (req, res) => {
    res.redirect("https://gitdab.com/y/shyzu.link");
  });
  app.get("/homepage", async (req, res) => {
    res.redirect("https://gitdab.com/y/website");
  });
  app.get("/yugen", async (req, res) => {
    res.redirect("https://yugen.work");
  });
  app.get("/thaldrin", async (req, res) => {
    res.redirect("https://thaldr.in/source");
  });
  app.get("/kaito", async (req, res) => {
    res.redirect("https://github.com/codepupper/kaito");
  });
  app.get("/yiff", async (req, res) => {
    res.redirect("https://npm.im/yiff");
  });
  app.get("/sor", async (req, res) => {
    res.redirect("https://sor.dog");
  });
    app.get("/benji", async (req, res) => {
    res.redirect("https://benji.monster");
  });

  app.get("/update", async (req, res) => {
    //console.log(req.headers['user-agent']);
    if (req.headers.authorization === con.updateToken) {
      await exec("git pull")
        .then(r => {
          console.log("Pulled latest changes");
          res.status(200).jsonp({
            success: true,
            message: "Successfully pulled latest changes"
          });
          process.exit();
          //trying.edit('```fix\n' + r.stdout + '```');
        })
        .catch(error => {
          res.status(400).jsonp({
            success: false,
            message: error.message
          });
          console.error(error);
        });
    } else {
      res.status(400).jsonp({
        success: false,
        message: "You are not authorized to access this Endpoint"
      });
    }
  });

  app.listen(port /* , hostname */, () => {
    console.log(`${chalk.blue("[ Server ]")} Listening on ${port}`);
  });
};
